function playSound (e) {
  if (e.type === 'touchstart') {
    e.preventDefault()

    const audio = document.querySelector(`audio[data-key='${e.currentTarget.dataset.key}']`)
    if (!audio) return

    e.currentTarget.classList.add('playing')
    audio.currentTime = 0
    audio.play()
  }

  const audio = document.querySelector(`audio[data-key='${e.keyCode}']`)
  const key = document.querySelector(`div[data-key='${e.keyCode}']`)
  if (!audio) return

  key.classList.add('playing')
  audio.currentTime = 0
  audio.play()
}
