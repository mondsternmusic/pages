// Checking if user-agent has touch support. '!navigator.keyboard' to check for keyboard support; experimental feature.
if (navigator.maxTouchPoints/* && !navigator.keyboard */) {
  const kbds = Array.from(document.querySelectorAll('kbd'))
  kbds.forEach(kbd => kbd.classList.add('hidden'))

  const keys = Array.from(document.querySelectorAll('.key'))
  keys.forEach(key => key.classList.add('taller'))

  const sounds = Array.from(document.querySelectorAll('.sound'))
  sounds.forEach(sound => sound.classList.add('bigger'))
}

// Adding touch support to keys, and instructing window to listen for keydown.
const keys = Array.from(document.querySelectorAll('.key'))
keys.forEach(key => {
  key.addEventListener('transitionend', removeTransition)
  key.addEventListener('touchstart', playSound)
})

// Listening for keydown events.
window.addEventListener('keydown', playSound)
